# Google Cloud Storage Trigger Function

This function will trigger when a file is uploaded to a particular google storage bucket. Once triggered it will copy the uploaded file to another bucket.

To deploy the function run:

`gcloud beta functions deploy copy_file --runtime python37 --trigger-resource [BUCKET_NAME] --trigger-event google.storage.object.finalize`

To view the logs:

`gcloud functions logs read --limit 50`