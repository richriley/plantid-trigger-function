from google.cloud import storage

SOURCE_BUCKET='plant-id-d1074.appspot.com'
DESTINATION_BUCKET='plant-id-d1074-vcm'

def copy_file(data, context):
    storage_client = storage.Client()

    full_path = data['name']

    if full_path.startswith('prod/'):
        file_name = full_path[5:]
        source_bucket = storage_client.get_bucket(SOURCE_BUCKET)

        destination_bucket = storage_client.get_bucket(DESTINATION_BUCKET)

        source_blob = source_bucket.blob(full_path)

        if full_path[-5:] != '.jpeg':
            new_blob = source_bucket.copy_blob(source_blob, source_bucket, 'prod/' +  file_name + '.jpeg')
            source_blob.delete()
            print('{} copied from {} to {} at {}'.format(full_path, SOURCE_BUCKET, SOURCE_BUCKET, data['timeCreated']))
        else:
            new_blob = source_bucket.copy_blob(source_blob, destination_bucket, file_name)
            print('{} copied from {} to {} at {}'.format(full_path, SOURCE_BUCKET, DESTINATION_BUCKET, data['timeCreated']))
